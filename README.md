# Solar client example
This is a sample client for using Rexplorer API-s

Full API documentation is available at https://api.rexplorer.ee/v1/docs

## Running
Tested with Python 3.10

Before running, create an .env file in the project directory, set you client credentials and api key there (see .env.example)

Run the example: `python main.py`
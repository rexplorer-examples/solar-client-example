import os
import requests
from dotenv import load_dotenv

load_dotenv()
CLIENT_ID = os.getenv('CLIENT_ID')
CLIENT_SECRET = os.getenv('CLIENT_SECRET')
API_KEY = os.getenv('API_KEY')

TOKEN_ENDPOINT = 'https://api.rexplorer.ee/connect/oauth2/token'
SOLAR_POTENTIAL_ENDPOINT = 'https://api.rexplorer.ee/v1/solar-potential'

access_token = None


def update_token():
    global access_token

    params = {
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
        "grant_type": "client_credentials",
    }

    response = requests.post(TOKEN_ENDPOINT, data=params)
    response.raise_for_status()

    access_token = response.json()['access_token']


def get_solar_potential_by_address(request_body):
    return get_solar_potential('/by-address', request_body)


def get_solar_potential_by_ehr_code(ehr_code):
    return get_solar_potential('/by-ehr-code', {'ehrCode': ehr_code})


def get_solar_potential(path, body_json):
    def do_get():
        headers = {
            "Authorization": "Bearer {token}".format(token=access_token),
            "Content-Type": "application/json",
            "x-api-key": API_KEY,
        }
        return requests.post(SOLAR_POTENTIAL_ENDPOINT + path, headers=headers, json=body_json)

    response = do_get()
    # If the token expired, refresh it and try the request again
    while response.status_code == 401:
        update_token()
        response = do_get()

    # Raise an exception on response errors at this point
    response.raise_for_status()

    return response


def german_building_example():
    request = {
        "address": "Wasserburger Landstraße 133, 81827 Munich, Germany",
        "countryCode": "DEU",
        "includeGeoReferencedModels": True,
        "arrangementConfiguration": {
            "panelDefinitions": [
                {
                    "height": 1.0,
                    "width": 2.0,
                    "output": 450.0
                }
            ],
            "flatRoofPanelOrientation": "EastWest",
            "flatRoofSlopeThreshold": 10,
            "roofEdgeMargin": 0.25,
            "panelArraySpacing": 0.3,
            "eastWestAxisDeviationThreshold": 45
        }
    }

    r = get_solar_potential_by_address(request)
    print(r.json())


def tallinnovation_example():
    request = {
        "address": "Tulika 9-11, Tallinn",
        "countryCode": "DEU",
        "useOwnModels": True,
        "includeGeoReferencedModels": True,
        "arrangementConfiguration": {
            "panelDefinitions": [
                {
                    "height": 1.0,
                    "width": 2.0,
                    "output": 450.0
                }
            ],
            "flatRoofPanelOrientation": "South"
        }
    }

    r = get_solar_potential_by_address(request)
    print(r.json())


def get_by_ehr_code_example():
    input_ehr_code = '116016462'
    r = get_solar_potential_by_ehr_code(input_ehr_code)
    print(f"Solar potential for EHR code {input_ehr_code}:")
    print(r.json())


if __name__ == '__main__':

    # tallinnovation_example()

    german_building_example()

    # get_by_ehr_code_example()
